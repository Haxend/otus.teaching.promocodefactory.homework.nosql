﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Cache;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly ICacheService _cacheService;

        public CustomersController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository, 
            ICacheService cacheService)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _cacheService = cacheService;
        }
        
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers =  await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer =  await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);

            return Ok(response);
        }
        
        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await GetPreferences(request.PreferenceIds);
            
            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            
            await _customerRepository.AddAsync(customer);

            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customer.Id}, customer.Id);
        }
        
        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <param name="request">Данные запроса></param>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();

            var preferences = await GetPreferences(request.PreferenceIds);
            
            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }

        private async Task<IEnumerable<Preference>> GetPreferences(List<Guid> preferenceIds)
        {
            var result = new List<Preference>();
            
            var preferenceIdsString = preferenceIds.Select(p => p.ToString()).ToArray();
            
            var dataFromCache = await _cacheService.GetStringListAsync(preferenceIdsString);
            if (dataFromCache.Any())
            {
                var deserializeDataFromCache = dataFromCache
                    .Select(p => JsonSerializer.Deserialize<Preference>(p.Item2)).ToList();
                result.AddRange(deserializeDataFromCache);
            }
            
            var keysFromCache = dataFromCache.Select(p => p.Item1).ToArray();
            var missingKeys = preferenceIds.Where(p => !keysFromCache.Contains(p.ToString())).ToList();
            
            if (missingKeys.Any())
            {
                var preferencesFromDb = (List<Preference>) await _preferenceRepository.GetRangeByIdsAsync(missingKeys);
                await UpdateCacheAsync(preferencesFromDb);
                var missingData = preferencesFromDb.Where(p => missingKeys.Contains(p.Id)).ToList();
                result.AddRange(missingData);
            }
            
            return result;
        }

        private Task UpdateCacheAsync(List<Preference> missingData)
        {
            var dataForUpdateCache = missingData.Select(p => (p.Id.ToString(), JsonSerializer.Serialize(p)));
            return _cacheService.UpdateCacheAsync(dataForUpdateCache);
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return NoContent();
        }
    }
}