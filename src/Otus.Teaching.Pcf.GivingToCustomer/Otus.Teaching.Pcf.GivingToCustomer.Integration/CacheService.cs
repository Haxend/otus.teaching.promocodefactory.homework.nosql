using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Cache;
using static System.String;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class CacheService : ICacheService
    {
        private readonly IDistributedCache _cache;

        public CacheService(IDistributedCache cache)
        {
            _cache = cache;
        }
        
        public async Task<List<(string, string)>> GetStringListAsync(IEnumerable<string> keys)
        {
            var result = new List<(string, string)>();
            foreach (var key in keys)
            {
                var stringFromCache = await _cache.GetStringAsync(key);
                if (!IsNullOrEmpty(stringFromCache))
                {
                    result.Add((key, stringFromCache));
                }
            }

            return result;
        }

        public async Task UpdateCacheAsync(IEnumerable<(string, string)> data)
        {
            foreach (var d in data)
            {
                await UpdateCacheAsync(d.Item1, d.Item2);
            }
        }
        
        private Task UpdateCacheAsync(string key, string value)
        {
            return _cache.SetStringAsync(
                key: key, 
                value: value, 
                options: new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10)
                });
        }
    }
}